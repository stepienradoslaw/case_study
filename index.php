<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
use Lpp\Entity\Collection;

include (__DIR__."/src/SplClassLoader.php");
$classLoader = new SplClassLoader();
$classLoader->loadClass('Lpp/Service/CollectionServiceInterface');
$classLoader->loadClass('Lpp/Service/BrandServiceInterface');
$classLoader->loadClass('Lpp/Service/ItemServiceInterface');
$classLoader->loadClass('Lpp/Service/PriceServiceInterface');
$classLoader->loadClass('Lpp/Entity/Item');
$classLoader->loadClass('Lpp/Entity/Price');
$classLoader->loadClass('Lpp/Entity/Collection');
$classLoader->loadClass('Lpp/Entity/Brand');
$collection = new Collection();

$brands = $collection->getBrands();
if ($brands && count($brands)) {
    foreach ($brands as $brand) {
        echo 'Brand ' . $brand->getId() . ' name: ' . $brand->getName();
        echo '<br>';
        echo 'Brand ' . $brand->getId() . ' description: ' . $brand->getDescription();
        echo '<br><br>';

        $items = $brand->getItems();
        if ($items && count($items)) {
            echo 'Items from brand ' . $brand->getName() . ': <br>';
            foreach ($items as $item) {
                echo 'Item ' . $item->getId() . ' name: ' . $item->getName();
                echo '<br>';
                echo 'Item ' . $item->getId() . ' url: ' . $item->getUrl();
                echo '<br><br>';

                $prices = $item->getPrices();
                if ($prices && count($prices)) {
                    echo 'Prices from item ' . $item->getName() . ': <br>';
                    foreach ($prices as $price) {
                        echo 'Price ' . $price->getId() . ' description: ' . $price->getDescription();
                        echo '<br>';
                        echo 'Price ' . $price->getId() . ' price in euro: ' . $price->getPriceInEuro() . '€';
                        echo '<br>';
                        echo 'Price ' . $price->getId() . ' arrival date: ' . $price->getArrival();
                        echo '<br>';
                        echo 'Price ' . $price->getId() . ' due to date: ' . $price->getDue();
                        echo '<br>';
                        echo '<br>';
                    }
                    echo '<br>';
                }
            }
        echo '<br>';
        }
    }
}
