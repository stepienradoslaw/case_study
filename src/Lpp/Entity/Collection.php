<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Entity;

use Lpp\Service\CollectionServiceInterface;

/**
 * Class Collection
 * @package Lpp\Entity
 */
class Collection implements CollectionServiceInterface
{
    const SORTING_NAME_FIELD = "name";
    const SORTING_DESCRIPTION_FIELD = "description";

    /**
     * Maps from collection name to the id for the item service.
     *
     * @var []
     */
    private $collectionNameToIdMapping = [
        "winter" => 1315475
    ];

    /**
     * @var string
     */
    public $json;

    /**
     * @var Brand[]
     */
    public $brands;

    /**
     * Collection constructor.
     */
    public function __construct()
    {
        $this->getBrandsForCollection(
            "winter",
            true,
            self::SORTING_NAME_FIELD,
            true
        );
    }

    /**
     * @param $jsonData
     * @return Collection|void
     */
    public function setBrands($jsonData)
    {
        foreach ($jsonData['brands'] as $id => $brandData) {
            $brand = new Brand();
            $brand->setId($id);
            $brand->setName($brandData['name']);
            $brand->setDescription($brandData['description']);
            $brand->setItems($brandData['items']);
            $this->brands[] = $brand;
        }
    }

    /**
     * @return Brand[]
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * @param string $collectionName Name of the collection to search for.
     *
     * @param bool $isSort
     * @param bool $sortField
     * @param bool $ascDirection
     * @return Brand[]
     */
    public function getBrandsForCollection($collectionName, $isSort = false, $sortField = false, $ascDirection = false) {
        if (empty($this->collectionNameToIdMapping[$collectionName])) {
            throw new \InvalidArgumentException(sprintf('Provided collection name [%s] is not mapped.', $collectionName));
        }

        $collectionId = $this->collectionNameToIdMapping[$collectionName];
        return $this->getResultForCollectionId($collectionId, $isSort, $sortField, $ascDirection);
    }

    /**
     * @param $brandName
     * @return Brand|bool
     */
    public function getBrandByName($brandName)
    {
        if ($brandName != null) {
            foreach ($this->brands as $brand) {
                if ($brand->getName() == $brandName) {
                    return $brand;
                }
            }
        }
        return false;
    }

    /**
     * This method should read from a datasource (JSON for case study)
     * and should return an unsorted list of brands found in the datasource.
     *
     * @param int $collectionId
     *
     * @param bool $isSort
     * @param bool $sortField
     * @param bool $ascDirection
     * @return Brand[]
     */
    public function getResultForCollectionId($collectionId, $isSort = false, $sortField = false, $ascDirection = true)
    {
        $fileContent = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/data/' . $collectionId . '.json');
        $this->json = json_decode($fileContent, true);
        $this->brands = null;
        if ($this->json['id'] == $collectionId) {
            $this->setBrands($this->json);
        }
        if ($isSort) {
            $this->sortBrandsResults($sortField, $ascDirection);
        }
        return $this->brands;
    }

    /**
     * Sort Brands[] in specified order by specified field, no need to prepare for integer fields so works
     * only with string type fields
     *
     * @param $sortField
     * @param $ascDirection
     * @return mixed|void
     */
    public function sortBrandsResults($sortField, $ascDirection)
    {
        usort($this->brands, function ($first, $second) use(&$sortField, &$ascDirection) {
            return $ascDirection
                ? strtolower($first->$sortField) > strtolower($second->$sortField)
                : strtolower($first->$sortField) < strtolower($second->$sortField);
        });
    }


    /**
     * @param string $brandName
     * @return Item[]|bool
     */
    public function getItemsByBrandName($brandName)
    {
        if ($brandName != null) {
            foreach ($this->brands as $brand) {
                if ($brand->getName() == $brandName) {
                    return $brand->getItems();
                }
            }
        }
        return false;
    }
}
