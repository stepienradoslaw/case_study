<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Entity;

use DateTime;
use Lpp\Service\PriceServiceInterface;

/**
 * Represents a single price from a search result
 * related to a single item.
 *
 * Class Price
 * @package Lpp\Entity
 */
class Price implements PriceServiceInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * Description text for the price
     * 
     * @var string
     */
    public $description;

    /**
     * Price in euro
     * 
     * @var int
     */
    public $priceInEuro;

    /**
     * Warehouse's arrival date (to)
     *
     * @var DateTime
     */
    public $arrivalDate;

    /**
     * Due to date,
     * defining how long will the item be available for sale (i.e. in a collection)
     *
     * @var DateTime
     */
    public $dueDate;

    /**
     * @param $id
     * @return Price|void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|void
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $description
     * @return Price|void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $priceInEuro
     * @return Price|void
     */
    public function setPriceInEuro($priceInEuro)
    {
        $this->priceInEuro = $priceInEuro;
    }

    /**
     * @return int
     */
    public function getPriceInEuro()
    {
        return $this->priceInEuro;
    }

    /**
     * @param $arrival
     * @return Price|void
     */
    public function setArrival($arrival)
    {
        $this->arrivalDate = $arrival;
    }

    /**
     * @return DateTime|string
     */
    public function getArrival()
    {
        return $this->arrivalDate;
    }

    /**
     * @param $due
     * @return Price|void
     */
    public function setDue($due)
    {
        $this->dueDate = $due;
    }

    /**
     * @return DateTime|string
     */
    public function getDue()
    {
        return $this->dueDate;
    }
}
