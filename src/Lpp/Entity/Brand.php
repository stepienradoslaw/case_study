<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Entity;

use Lpp\Service\BrandServiceInterface;
use Lpp\Service\ItemServiceInterface;

/**
 * Represents a single brand in the result.
 *
 * Class Brand
 * @package Lpp\Entity
 */
class Brand implements BrandServiceInterface
{
    /**
     * Name of the brand
     *
     * @var string
     */
    public $name;

    /**
     * Brand's description
     * 
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $id;

    /**
     * Unsorted list of items with their corresponding prices.
     * 
     * @var Item[]
     */
    public $items = [];

    /**
     * @param $id
     * @return Brand|void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return Brand|void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $description
     * @return Brand|void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $itemsData
     * @return Brand|void
     */
    public function setItems($itemsData)
    {
        if (count($itemsData)) {
            foreach ($itemsData as $id => $itemData) {
                $item = new Item();
                $item->setId($id);
                $item->setName($itemData['name']);
                $item->setUrl($itemData['url']);
                $item->setPrices($itemData['prices']);
                $this->items[] = $item;
            }
        }
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param ItemServiceInterface $ItemService
     */
    public function setItemService(ItemServiceInterface $ItemService)
    {
        //Nothing to do here
    }
}
