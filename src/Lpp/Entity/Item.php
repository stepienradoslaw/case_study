<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Entity;

use Lpp\Service\ItemServiceInterface;

/**
 * Represents a single item from a search result.
 *
 * Class Item
 * @package Lpp\Entity
 */
class Item implements ItemServiceInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * Name of the item
     *
     * @var string
     */
    public $name;

    /**
     * Url of the item's page
     * 
     * @var string
     */
    public $url;

    /**
     * Unsorted list of prices received from the 
     * actual search query.
     * 
     * @var Price[]
     */
    public $prices = [];

    /**
     * @param $id
     * @return Item|void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return Item|void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $url
     * @return string|void
     */
    public function setUrl($url)
    {
        if ($this->isValidUrl($url)) {
            $this->url = $url;
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param $pricesData
     * @return Price|void
     */
    public function setPrices($pricesData)
    {
        if (count($pricesData)) {
            foreach ($pricesData as $id => $priceData) {
                $price = new Price();
                $price->setId($id);
                $price->setDescription($priceData['description']);
                $price->setPriceInEuro($priceData['priceInEuro']);
                $price->setArrival($priceData['arrival']);
                $price->setDue($priceData['due']);
                $this->prices[] = $price;
            }
        }
    }

    /**
     * @return Price[]
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param $url
     * @return bool
     */
    public function isValidUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }
}
