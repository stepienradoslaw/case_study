<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Service;

use Lpp\Entity\Item;
use Lpp\Entity\Price;

/**
 * Interface ItemServiceInterface
 * @package Lpp\Service
 */
interface ItemServiceInterface
{
    /**
     * @param $id
     * @return Item
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param $name
     * @return Item
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $url
     * @return string
     */
    public function setUrl($url);

    /**
     * @return string
     */
    public function getUrl();

    /**
     * @param $pricesData
     * @return Price
     */
    public function setPrices($pricesData);

    /**
     * @return Price[]
     */
    public function getPrices();

    /**
     * @param $url
     * @return bool
     */
    public function isValidUrl($url);
}
