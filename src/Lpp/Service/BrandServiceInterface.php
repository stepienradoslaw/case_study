<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Service;

use Lpp\Entity\Brand;
use Lpp\Entity\Item;

/**
 * Interface BrandServiceInterface
 * @package Lpp\Service
 */
interface BrandServiceInterface
{
    /**
     * @param $id
     * @return Brand
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param $name
     * @return Brand
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $description
     * @return Brand
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param $itemData
     * @return Brand
     */
    public function setItems($itemData);

    /**
     * @return Item[]
     */
    public function getItems();

    /**
     * This is supposed to be used for testing purposes.
     * You should avoid replacing the item service at runtime.
     *
     * @param ItemServiceInterface $ItemService
     * @return void
     */
    public function setItemService(ItemServiceInterface $ItemService);
}
