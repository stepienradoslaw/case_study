<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Service;

use DateTime;
use Lpp\Entity\Price;

/**
 * Interface PriceServiceInterface
 * @package Lpp\Service
 */
interface PriceServiceInterface
{
    /**
     * @param $id
     * @return Price
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param $description
     * @return Price
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param $priceInEuro
     * @return Price
     */
    public function setPriceInEuro($priceInEuro);

    /**
     * @return int
     */
    public function getPriceInEuro();

    /**
     * @param $arrival
     * @return Price
     */
    public function setArrival($arrival);

    /**
     * @return DateTime
     */
    public function getArrival();

    /**
     * @param $due
     * @return Price
     */
    public function setDue($due);

    /**
     * @return DateTime
     */
    public function getDue();
}
