<?php
/**
 * @author Radosław Stępień <stepien.radoslaw1@o2.pl>
 */
namespace Lpp\Service;

use Lpp\Entity\Brand;
use Lpp\Entity\Collection;
use Lpp\Entity\Item;

/**
 * The implementation is responsible for resolving the id of the collection from the
 * given collection name.
 *
 * Second responsibility is to sort the returning result from the item service in whatever way.
 *
 * Please write in the case study's summary if you find this approach correct or not. In both cases explain why.
 *
 * Interface CollectionServiceInterface
 * @package Lpp\Service
 */
interface CollectionServiceInterface
{
    /**
     * @param $jsonData
     * @return Collection|void
     */
    public function setBrands($jsonData);

    /**
     * @return Brand[]
     */
    public function getBrands();

    /**
     * @param string $collectionName Name of the collection to search for.
     *
     * @param $isSort
     * @param $sortField
     * @param $sortDirection
     * @return Brand[]
     */
    public function getBrandsForCollection($collectionName, $isSort, $sortField, $sortDirection);

    /**
     * @param $brandName
     * @return Brand|bool
     */
    public function getBrandByName($brandName);

    /**
     * This method should read from a datasource (JSON for case study)
     * and should return an unsorted list of brands found in the datasource.
     *
     * @param int $collectionId
     *
     * @param $isSort
     * @param $sortField
     * @param $ascDirection
     * @return Brand[]
     */
    public function getResultForCollectionId($collectionId, $isSort, $sortField, $ascDirection);

    /**
     * @param string $brandName Name of a brand to search for
     *
     * @return Item[]
     */
    public function getItemsByBrandName($brandName);

    /**
     * Method for sort brands results in specified way
     * @param $sortField
     * @param $ascDirection
     * @return mixed
     */
    public function sortBrandsResults($sortField, $ascDirection);
}
